#!/bin/bash

# prerequisites:
# - current directory contains executables
#   * params.template
#	* coords.inp

DIR=results
BUILDDIR=build
mkdir -p $DIR
mkdir -p $BUILDDIR

cd $BUILDDIR
for METH in NORMAL PAIR_LIST LINKED_LIST GRID_CELL
do
	# Compile the program for different methods
    rm -rf *
	cmake -D${METH}=ON ..
	make -j 8
	mv mdatom ../${DIR}/mdatom_$METH
done

cd ../${DIR}
mkdir -p params
mkdir -p output
for NATOMS in 100 200 500 1000 2000 5000 #10000 #20000 50000 100000 200000 500000 1000000
do
	# Create parameter file
	PARAM_FILE=params_$NATOMS.inp
	sed s/_NumberAtoms_/$NATOMS/ < ../params.template > params/$PARAM_FILE

	# Perform calculations for the four methods
	# echo "Computing with ${NATOMS} atoms:"
	for METH in NORMAL PAIR_LIST LINKED_LIST GRID_CELL
	do
		OUTPUT_FILE=${METH}_${NATOMS}.out
		# echo "-> Method: ${METH}"
		gnome-terminal -x sh -c "echo ${METH}_${NATOMS} && ./mdatom_$METH params/$PARAM_FILE > output/$OUTPUT_FILE"
		# echo "---> Done"
	done
	# echo ""
	# echo "----------------------------"
	# echo ""
done
cd ..
