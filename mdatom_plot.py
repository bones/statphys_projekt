import re
import matplotlib.pyplot as plt
import numpy as np

# Lists of all possible methods and number of atoms
meth = ['NORMAL','PAIR_LIST','LINKED_LIST','GRID_CELL']
natoms = [100,200,500,1000,2000,5000]#,10000]#,20000,50000,100000,200000,500000,1000000]

# Number of elements of natoms
nposs = len(natoms)

# For every output file save the timing in the list time
pattern = re.compile('Time spent for MD iterations')
time = []
for m in meth:
	for n in natoms:	
		f = open('results/output/' + m + '_' + str(n) + '.out','r')
		
		while 1:
			line = f.readline()
			if not line:
				raise EOFError
			if pattern.search(line) != None:
				tmp = ""
				
				for i in range(32, 50):
					if line[i] == ' ':
						break
					tmp += line[i]
				
				time.append(float(tmp))
				break

# -Convert python-lists natoms and time in numpy-array
# -Divide the time array in different arrays for each method
n = np.array(natoms)
t1 = np.array(time[0:nposs])				# Normal
t2 = np.array(time[nposs:2*nposs])			# Pair List
t3 = np.array(time[2*nposs:3*nposs])		# Linked List
t4 = np.array(time[3*nposs:4*nposs])		# Grid Cell

# Plot the three lines. One for each method
fig, ax = plt.subplots()
ax.plot(n,t1,'b-',label='Normal')
ax.plot(n,t2,'r-.',label='Pair List')
ax.plot(n,t3,'g--',label='Linked List')
ax.plot(n,t4,'y:',label='Grid Cell')

# Set aspect of the plot: auto --> fill the position rectangle with data
ax.set_aspect('auto')

# Create legend in the upper-left corner
ax.legend(loc=2)

# Set the axes scale as logarithmic
ax.set_xscale('log')
ax.set_yscale('log')

# Set title and labels of the axes
ax.set_title('Searching Neighbors Timings')
ax.set_xlabel('Number of atoms')
ax.set_ylabel('Time')

# Save plot as pdf
fig.savefig('results/timing_graph.pdf')
plt.close(fig)
