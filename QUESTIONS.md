# Questions for TA

 - What parameters are realistic? For what parameters is the simulation expected to be correct?
 - Floating-point addition can have different results if the summands are given in different order. How relevant is this to this simulation? What kinds of errors could be expected from this phenomenon?
 - Some tests are passed without issue, others give _all_ of the methods problems. For instance, test 7 (parameters under /test/input_files/ex7/params.inp) always seems to fail. The major difference with the others is that it has a very large box size (100x100x100 units) and only 1000 atoms, resulting in very low "density". Could this be the cause of the errors?
 - So far, none of our implementations have _exactly_ reproduced the results of the "normal" simulation. When testing for correctness, we check if the relative error is below a certain threshold (currently 1%). Is this the correct approach?