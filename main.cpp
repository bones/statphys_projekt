#include <iostream>

#include <MDSimulation.h>

/*
 * Preprocessor options:
 * PAIR_LIST -> use atom pair-list method (method 1)
 * LINKED_LIST -> use linked-list technique (method 2)
 * GRID_CELL -> use grid-cell technique (method 3)
 * 
 * If none of these is defined, uses the default std::vector approach.
 * 
*/

/*!
 * main function.
 * The program expects one argument for the input file (parameters), an one optional argument for
 * the  file with the initial coordinates.
 */

int main(int argc, char* argv[]) {
    switch (argc) {
        case 2: break;
        case 3: break;
        default:
        std::cerr << "Usage: mdatom input_file [coordinate_file] > output \n";
            return 1;
    }

    std::string parameterFile = argv[1];
    std::string coordinatesFile; // NB: might be empty
    if (argc > 2)
        coordinatesFile = argv[2];
    
    // Determine data structure to use for positions in simulation:
    
    #ifdef PAIR_LIST
        std::cout << "ATOM PAIR LIST!!" << std::endl;
	MDS_pairlist md(std::cout);
    #elif defined LINKED_LIST
        std::cout << "LINKED LIST!!" << std::endl;
	MDS_ll md(std::cout);
    #elif defined GRID_CELL
        std::cout << "GRID CELL!!" << std::endl;
	MDS_gc md(std::cout);
    #else
        std::cout << "STD::VECTOR DOUBLE!!" << std::endl;
	MDSimulation md(std::cout);
    #endif

    // MDS_pairlist md(std::cout);
    
    try {
        md.performSimulation(parameterFile, coordinatesFile);
    }
    catch (std::exception& e) {
        std::cerr << e.what();
        return 1;
    }

    return 0;
}
