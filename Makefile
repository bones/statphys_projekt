
# Makefile for easily compiling library with different methods

# Build normal
.PHONY: bn
bn:
	mkdir -p build_n
	cd build_n && cmake .. && $(MAKE) --no-print-directory -j8

# Build pair-list method
.PHONY: bpl
bpl:
	mkdir -p build_pl
	cd build_pl && cmake -DPAIR_LIST=ON .. && $(MAKE) --no-print-directory -j8

# Build linked-list method
.PHONY: bll
bll:
	mkdir -p build_ll
	cd build_ll && cmake -DLINKED_LIST=ON .. && $(MAKE) --no-print-directory -j8

# Build linked-list method for gprof
.PHONY: llpg
llpg:
	mkdir -p build
	cd build && cmake -DCMAKE_CXX_FLAGS=-pg -DLINKED_LIST=ON .. && $(MAKE) --no-print-directory -j8


# Build grid-cell method
.PHONY: bgc
bgc:
	mkdir -p build_gc
	cd build_gc && cmake -DGRID_CELL=ON .. && $(MAKE) --no-print-directory -j8


# Run tests on whatever is built (parallel)
.PHONY: ptest
ptest:
	@echo "\n\n\033[01;32m---> Running tests for Pair-List method\033[0m"
	@-if [ -d "build_pl" ]; then cd build_pl && ctest --output-on-failure --rerun-failed -j8; else echo "Skipping (no build files)."; fi
	@echo "\n\n\033[01;32m---> Running tests for Linked-List method\033[0m"
	@-if [ -d "build_ll" ]; then cd build_ll && ctest --output-on-failure --rerun-failed -j8; else echo "Skipping (no build files).";  fi
	@echo "\n\n\033[01;32m---> Running tests for Grid-Cell method\033[0m"
	@-if [ -d "build_gc" ]; then cd build_gc && ctest --output-on-failure --rerun-failed -j8; else echo "Skipping (no build files).";  fi

.PHONY: ptest_pl
ptest_pl:
	@echo "\n\n\033[01;32m---> Running tests for Pair-List method\033[0m"
	cd build_pl && ctest --output-on-failure --rerun-failed -j8

.PHONY: ptest_ll
ptest_ll:
	@echo "\n\n\033[01;32m---> Running tests for Linked-List method\033[0m"
	cd build_ll && ctest --output-on-failure --rerun-failed -j8

.PHONY: ptest_gc
ptest_gc:
	@echo "\n\n\033[01;32m---> Running tests for Grid-Cell method\033[0m"
	cd build_gc && ctest --output-on-failure --rerun-failed -j8



# Run calculate.sh
.PHONY: calc
calc:
	./calculate.sh

# Plot the graphs
# * Requires "make calc"
.PHONY: plot
plot:
	python3 mdatom_plot.py

# Run tests on whatever is built (sequentially)
.PHONY: test
test:
	echo "\n\nRunning tests for Pair-List method"
	cd build_pl && ctest --output-on-failure --rerun-failed
	echo "\n\nRunning tests for Linked-List method"
	cd build_ll && ctest --output-on-failure --rerun-failed
	echo "\n\nRunning tests for Grid-Cell method"
	cd build_gc && ctest --output-on-failure --rerun-failed

# Remove entire build dir
.PHONY: clean
clean:
	rm -rf build/
	rm -rf build_n/
	rm -rf build_pl/
	rm -rf build_ll/
	rm -rf build_gc/
