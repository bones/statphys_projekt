#ifndef MDRUN_GC_H
#define MDRUN_GC_H

#include "MDRun.h"

// Grid-cell method
class MDRun_gc : public MDRun {
	public:
		MDRun_gc(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter);
		void run(std::vector<double> &x, std::vector<double> &v);

	private:
		inline void resetCells();
		void fillCells(const std::vector<double> &x);
		unsigned int computeCellFromAtom(const std::vector<double>& x, const unsigned int& atom);
		void calculateForces(std::vector<double>& x);

		// Vector of cells
		std::vector<int> cells;
		unsigned int ncells; // Total number of cells
		unsigned int Lcells[3]; // Number of cells along each axis
		double rcell[3]; // Dimensions of individual cell
		int emptyCellFlag = -1; // Flag for an empty cell
		unsigned int cellRange[3]; // Interaction cutoff counted in cells
};

#endif // MDRUN_GC_H
