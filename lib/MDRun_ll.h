#ifndef MDRUN_LL_H
#define MDRUN_LL_H

#include "MDRun.h"

// Linked-list method
class MDRun_ll : public MDRun {
	public:
		using cellList_t = std::list<unsigned int>;

		MDRun_ll(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter);
		void run(std::vector<double> &x, std::vector<double> &v);
		unsigned int computeCellFromAtom(const std::vector<double>& x, const unsigned int& atom);
	private:
		void initializeCells(const std::vector<double>& x);
		void resetCells();
		void performStep(std::vector<double>& positions, std::vector<double>& velocities, int nstep, double time);
		void calculateForces(std::vector<double>& x);

		// Vector of cells (each cell is a linked-list)
		std::vector< cellList_t > cells;
		// Number of cells along each axis
		unsigned int Lcells[3];
		// Total number of cells
		unsigned int ncells;
		// Size of individual cells
		double rcell[3];
};

#endif // MDRUN_LL_H
