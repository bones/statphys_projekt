#ifndef MDRun_PL_H
#define MDRun_PL_H

#include "MDRun.h"

#include <forward_list>

// Pair-list method
class MDRun_pairlist : public MDRun {
	public:
		MDRun_pairlist(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter);
		void run(std::vector<double> &x, std::vector<double> &v);
	private:

		void calculateNeighbors(const std::vector<double> &x);
		double calculateSquaredDistance(const std::vector<double>& x, unsigned int i, unsigned int j);
		void calculateForces(std::vector<double>& x);

		// std::vector< std::vector<unsigned int> > neighbors;
		std::vector< std::forward_list<unsigned int> > neighbors;
};

#endif // MDRUN_PL_H
