#ifndef MDSIMULATION_H
#define MDSIMULATION_H

#include "MDRun.h"
#include "MDParameters.h"
#include "MDRunOutput.h"
#include "Timer.h"
#include "AveragedRadialDistribution.h"

#include <string>
#include <iostream>
#include <vector>

/*!
 * This class launches a MD simulation starting from parameters and, optionally, coordinates.
 */
class MDSimulation {
  public:
    /*! Constructor; the output of the MD simulation will be redirected to outputStream. */
    explicit MDSimulation(std::ostream& outputStream);

    /*! Perform a simulation based on a parameter file and an (optional) coordinate file. */
    void performSimulation(const std::string& parFile, const std::string& coordFile = "");
    /*! Perform a simulation based parameters and an (optional) coordinate file. */
    void performSimulation(const MDParameters& par, const std::string& coordinateFile = "");

  protected:
    void prepareRun();
    void checkParameterValidity();
    void initializeCoordinatesAndVelocities(const std::string& coordinateFile);
    virtual void executeMDIterations();
    void printRadialDistribution(const AveragedRadialDistribution& radialDistribution);
    void finalizeRun();

    MDRunOutput output;
    Timer timer;
    MDParameters parameters;
    std::vector<double> positions, velocities;
};


class MDS_pairlist : public MDSimulation {
	public:
		MDS_pairlist(std::ostream& os) : MDSimulation(os) {};
	private:
		void executeMDIterations();
};


class MDS_ll : public MDSimulation {
	public:
		MDS_ll(std::ostream& os) : MDSimulation(os) {};
	private:
		void executeMDIterations();
};

class MDS_gc : public MDSimulation {
	public:
		MDS_gc(std::ostream & os) : MDSimulation(os) {};
	private:
		void executeMDIterations();
};
#endif // MDSIMULATION_H
