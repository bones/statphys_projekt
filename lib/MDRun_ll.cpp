#include "MDRun_ll.h"
#include "PeriodicBoundaryConditions.h"
#include "CenterOfMassCalculator.h"
#include "TrajectoryFileWriter.h"

#include <iostream>
#include <cmath>

// Linked-list method
MDRun_ll::MDRun_ll(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter)
				: MDRun(parameters, out, trajectoryFileWriter) {
	// Set size of each cell, then compute number of cells
	for (unsigned i = 0; i < 3; ++i) {
		Lcells[i] = static_cast<int>(par.boxSize[i] / par.interactionCutoffRadius);
		rcell[i] = par.boxSize[i] / Lcells[i];
	}
	ncells = Lcells[0]*Lcells[1]*Lcells[2];
	cells.resize(ncells);
}

unsigned int MDRun_ll::computeCellFromAtom(const std::vector<double>& x, const unsigned int& atom) {
	// Cell coordinate vector
	unsigned int c[3];
	for (unsigned int j = 0; j < 3; ++j) {
		double dim = x[3*atom + j];
		c[j] = static_cast<int>(dim / rcell[j]) % Lcells[j];
	}

	// Cell index from vector
	return c[0] * Lcells[1] * Lcells[2] + c[1] * Lcells[2] + c[2];
}

void MDRun_ll::initializeCells(const std::vector<double>& x) {
	for (int i = 0; i < par.numberAtoms; i++) {
		// Add atom to cell
		unsigned cellIdx = computeCellFromAtom(x, i);
		cells[cellIdx].push_front(i);
	}
}

void MDRun_ll::resetCells() {
	for (auto cit = cells.begin(); cit != cells.end(); cit->clear(), cit++);
}

void MDRun_ll::run(std::vector<double> &x, std::vector<double> &v) {
    forces.resize(x.size());
    synchronizedPositions.resize(x.size());
    radialDistribution.setZero();

    initializeVariables();
    initializeTemperature(v);

    // Initialize cell lists
    initializeCells(x);

    output.printInitialTemperature(properties[1] / fac);
    output.printIterationStart();

    /* dynamics step */
    double time = par.initialTime;
    for (int nstep = 0; nstep < par.numberMDSteps; nstep++) {
        time += par.timeStep;
        performStep(x, v, nstep, time);
    }

    printAverages(time);
    std::cout << "Number of interactions calculated: " << forceCalculator.numInteractions << std::endl;
    std::cout << "Number of cells: " << ncells << std::endl;
}

void MDRun_ll::calculateForces(std::vector<double>& x) {
	forceCalculator.calculate(x, forces, cells, Lcells);
}

void MDRun_ll::performStep(std::vector<double>& positions, std::vector<double>& velocities, int nstep, double time) {
    /* put atoms in central periodic box */
    PeriodicBoundaryConditions::recenterAtoms(par.numberAtoms, positions, par.boxSize);

    /* calculate forces, potential energy, virial
     * and contribution to the radial distribution function
     */
    calculateForces(positions);
    radialDistribution.addInstantaneousDistribution(forceCalculator.getInstantaneousRadialDistribution());
    double vir = forceCalculator.getVirial();
    properties[2] = forceCalculator.getPotentialEnergy();
    properties[3] = vir;

    /* determine velocity scaling factor, when coupling to a bath */
    double scal = 1;
    if (par.mdType == SimulationType::constantTemperature) {
        double dtt = par.timeStep / par.temperatureCouplingTime;
        scal = std::sqrt(1 + dtt * (ekin0 / ekg - 1));
    }

    /* perform leap-frog integration step,
     * calculate kinetic energy at time t-dt/2 and at time t,
     * and calculate pressure
     */
    double oldKineticEnergy = 0.;
    double newKineticEnergy = 0.;
    for (int j3 = 0; j3 < nat3; j3++) {
        double oldVelocity = velocities[j3];
        double newVelocity = (oldVelocity + forces[j3] * dtm) * scal;
        oldKineticEnergy += newVelocity * newVelocity;
        newKineticEnergy += (oldVelocity + newVelocity) * (oldVelocity + newVelocity);
        velocities[j3] = newVelocity;
        positions[j3] += newVelocity * par.timeStep;
    }

    // Reset cells & fill again with new positions
    resetCells();
    initializeCells(positions);

    oldKineticEnergy *= (par.atomicMass / 2.);
    newKineticEnergy *= (par.atomicMass / 8.);
    properties[1] = newKineticEnergy;
    properties[0] = properties[1] + properties[2];
    double pres = 2. * (newKineticEnergy - vir) / (vol * 3.);
    properties[4] = pres;
    properties[5] = scal;
    if (par.mdType == SimulationType::constantTemperature) {
        ekg = oldKineticEnergy;
    }

    /* update arrays for averages and fluctuations */
    for (int m = 0; m < numberProperties; m++) {
        averages[m] += properties[m];
        fluctuations[m] += properties[m] * properties[m];
    }

    printOutputForStep(positions, velocities, nstep, time);
}

