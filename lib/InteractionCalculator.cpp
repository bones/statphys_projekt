#include "InteractionCalculator.h"
#include <iostream>
#include <cmath>

inline int nearestInteger(double x) {
    return x > 0 ? static_cast<int>(x + 0.5) : static_cast<int>(x - 0.5);
}

InteractionCalculator::InteractionCalculator(const MDParameters& parameters)
  : par(parameters),
    radialDistribution(parameters.numberRadialDistrPoints, parameters.radialDistrCutoffRadius) {
    initializeValues();

    numInteractions = 0;
}

void InteractionCalculator::initializeValues() {
    sig6 = par.sigmaLJ * par.sigmaLJ;
    sig6 = sig6 * sig6 * sig6;
    c6 = 4. * par.epsilonLJ * sig6;
    c12 = c6 * sig6;
    rcutf2 = par.interactionCutoffRadius * par.interactionCutoffRadius;
    for (int m = 0; m < 3; m++)
        inverseBoxLength[m] = 1.0 / par.boxSize[m];
}

void InteractionCalculator::calculate(const std::vector<double>& positions, std::vector<double>& forces) {
    resetVariablesToZero(forces);

    for (int i = 0; i < par.numberAtoms - 1; i++) {
        for (int j = i + 1; j < par.numberAtoms; j++) {
            calculateInteraction(i, j, positions, forces);
        }
    }
    virial /= 2.;
}


/********************************************************************************/
// Pair-list method:
void InteractionCalculator::calculate(const std::vector<double>& positions, std::vector<double>& forces,
					const std::vector<std::forward_list<unsigned int>>& neighbors) {
    resetVariablesToZero(forces);

    for (int i = 0; i < par.numberAtoms - 1; i++) {
        // for (unsigned int j = 0; j < neighbors[i].size(); j++) {
        //     calculateInteraction(i, neighbors[i][j], positions, forces);
        // }
	for (auto it = neighbors[i].begin(); it != neighbors[i].end(); it++) {
		calculateInteraction(i, *it, positions, forces);
	}
    }
    virial /= 2.;
}
/********************************************************************************/


/********************************************************************************/
// Cell linked-list method:

// Compute scalar cell index from cell vector
unsigned int getCellIdx(int const * cellvec, const unsigned int * Lcells) {
	return cellvec[0] * Lcells[1] * Lcells[2] + cellvec[1] * Lcells[2] + cellvec[2];
}

unsigned int getCellIdx(unsigned int const * cellvec, const unsigned int * Lcells) {
	return cellvec[0] * Lcells[1] * Lcells[2] + cellvec[1] * Lcells[2] + cellvec[2];
}

unsigned int getCellIdx(int const * cellvec, const int * Lcells) {
	return cellvec[0] * Lcells[1] * Lcells[2] + cellvec[1] * Lcells[2] + cellvec[2];
}

void InteractionCalculator::doInteractionsBetweenCells(unsigned int c, unsigned int nc,
						const std::vector< std::list<unsigned int> > & cells,
						const std::vector<double>& positions, std::vector<double>& forces) {
	// For each atom in central cell, compute interaction for each atom in neighbor cell
	for (auto atom1 = cells[c].begin(); atom1 != cells[c].end(); atom1++) {
	for (auto atom2 = cells[nc].begin(); atom2 != cells[nc].end(); atom2++) {
		// Avoid calculating same pair twice
		unsigned a1, a2; a1 = *atom1; a2 = *atom2;
		if (a1 < a2)
			calculateInteraction(a1, a2, positions, forces);
	}}
}


void InteractionCalculator::doNeighborCells(unsigned int * cell, unsigned int const * Lcells,
			const std::vector< std::list<unsigned int> > & cells, const std::vector<double>& positions,
			std::vector<double>& forces) {

	unsigned int c = getCellIdx(cell, Lcells);
	// Iterate neighbor cells of this cell
	int shift[3];
	for (shift[0]=-1; shift[0] <= +1; (shift[0])++) {
	for (shift[1]=-1; shift[1] <= +1; (shift[1])++) {
	for (shift[2]=-1; shift[2] <= +1; (shift[2])++) {
		int ncell[3];
		// Apply periodic boundary conditions to neighbor cells
		for (unsigned int i = 0; i < 3; i++)
			ncell[i] = (static_cast<int>(cell[i]) + shift[i]) % Lcells[i];

		unsigned int nc = getCellIdx(ncell, Lcells);
		
		doInteractionsBetweenCells(c, nc, cells, positions, forces);
	}}}
}

void InteractionCalculator::calculate(const std::vector<double>& positions, std::vector<double>& forces,
		   			const std::vector< std::list<unsigned int> > & cells, unsigned int const * Lcells) {
	
	resetVariablesToZero(forces);

	unsigned int Lz = Lcells[2];
	unsigned int Lyz = Lz * Lcells[1];

	// Iterate each cell
	unsigned int c[3];
	for (c[0] = 0; c[0] < Lcells[0]; (c[0])++) {
	for (c[1] = 0; c[1] < Lcells[1]; (c[1])++) {
	for (c[2] = 0; c[2] < Lcells[2]; (c[2])++) {
		// Cell index from vector
		unsigned int cell = c[0] * Lyz + c[1] * Lz + c[2];

		// Iterate each neighbor cell (including itself)
		int neighborv[3];
		for (neighborv[0] = (int) c[0]-1; neighborv[0] <= (int) c[0]+1; (neighborv[0])++) {
		for (neighborv[1] = (int) c[1]-1; neighborv[1] <= (int) c[1]+1; (neighborv[1])++) {
		for (neighborv[2] = (int) c[2]-1; neighborv[2] <= (int) c[2]+1; (neighborv[2])++) {
			unsigned int ncv[] = {
				(neighborv[0] + Lcells[0]) % Lcells[0],
				(neighborv[1] + Lcells[1]) % Lcells[1],
				(neighborv[2] + Lcells[2]) % Lcells[2]
			};
			//std::cout << neighborv[0] << "-" << ncv[0] << " ";

			unsigned int neighborCell = ncv[0] * Lyz + ncv[1] * Lz + ncv[2];

			for (auto cit = cells[cell].begin(); cit != cells[cell].end(); cit++) {
				for (auto nit = cells[neighborCell].begin(); nit != cells[neighborCell].end(); nit++) {
					if (*cit < *nit)
						calculateInteraction(*cit, *nit, positions, forces);
				}
			}
		}}}
	}}}

    virial /= 2.;
}

/********************************************************************************/


/********************************************************************************/
// Grid-cell method
void InteractionCalculator::calculate(const std::vector<double>& positions, std::vector<double>& forces, 
		const std::vector<int>& cells, int emptyCellFlag, unsigned int const * Lcells, unsigned int const * cellRange) {
	resetVariablesToZero(forces);

	unsigned int Lz = Lcells[2];
	unsigned int Lyz = Lz * Lcells[1];

	int cr[3];
	for (unsigned int i = 0; i < 3; i++)
		cr[i] = cellRange[i];
	
	unsigned int c[3];
	for (c[0] = 0; c[0] < Lcells[0]; (c[0])++) {
	for (c[1] = 0; c[1] < Lcells[1]; (c[1])++) {
	for (c[2] = 0; c[2] < Lcells[2]; (c[2])++) {
		unsigned int cell = c[0] * Lyz + c[1] * Lz + c[2];

		if (cells[cell] == emptyCellFlag)
			continue;
		
		int neighborv[3];
		for (neighborv[0] = (int) c[0] - cr[0]; neighborv[0] <= (int) c[0] + cr[0]; (neighborv[0])++) {
		for (neighborv[1] = (int) c[1] - cr[1]; neighborv[1] <= (int) c[1] + cr[1]; (neighborv[1])++) {
		for (neighborv[2] = (int) c[2] - cr[2]; neighborv[2] <= (int) c[2] + cr[2]; (neighborv[2])++) {
			unsigned int ncv[] = {
				(neighborv[0] + Lcells[0]) % Lcells[0],
				(neighborv[1] + Lcells[1]) % Lcells[1],
				(neighborv[2] + Lcells[2]) % Lcells[2]
			};

			unsigned int ncell = ncv[0] * Lyz + ncv[1] * Lz + ncv[2];

			if (cells[ncell] == emptyCellFlag)
				continue;
			else if (cells[cell] < cells[ncell])
				calculateInteraction(cells[cell], cells[ncell], positions, forces);
		}}}
		
	}}}

	virial /= 2.;
}

/********************************************************************************/


void InteractionCalculator::resetVariablesToZero(std::vector<double>& forces) {
    radialDistribution.setZero();
    potentialEnergy = 0;
    virial = 0;
    int nat3 = 3 * par.numberAtoms;
    for (int j3 = 0; j3 < nat3; j3++)
        forces[j3] = 0;
}

void InteractionCalculator::calculateInteraction(int i, int j, const std::vector<double>& positions,
                                                 std::vector<double>& forces) {
    applyPeriodicBoundaryConditions(i, j, positions);
    calculateSquaredDistance();
    if (rij2 < rcutf2) {
        calculatePotentialAndForceMagnitude();
        potentialEnergy += eij;
        calculateForceAndVirialContributions(i, j, forces);
numInteractions++;
    }
    radialDistribution.addPairAtSquaredDistance(rij2);
}

void InteractionCalculator::applyPeriodicBoundaryConditions(int i, int j, const std::vector<double>& positions) {
    int i3 = 3 * i;
    int j3 = 3 * j;
    for (int m = 0; m < 3; m++) {
        xij[m] = positions[i3 + m] - positions[j3 + m];
        xij[m] = xij[m] - nearestInteger(xij[m]*inverseBoxLength[m]) * par.boxSize[m];
    }
}

void InteractionCalculator::calculateSquaredDistance() {
    rij2 = 0;
    for (int m = 0; m < 3; m++)
        rij2 += xij[m] * xij[m];
}

void InteractionCalculator::calculatePotentialAndForceMagnitude() {
    double riji2 = 1.0 / rij2; // inverse inter-particle distance squared
    double riji6 = riji2 * riji2 * riji2; // inverse inter-particle distance (6th power)
    double crh = c12 * riji6;
    double crhh = crh - c6; //  L-J potential work variable
    eij= crhh * riji6;
    dij= 6. * (crh + crhh) * riji6 * riji2;
}

void InteractionCalculator::calculateForceAndVirialContributions(int i, int j, std::vector<double>& forces) {
    int i3 = 3 * i;
    int j3 = 3 * j;
    for (int m = 0; m < 3; m++) {
        // Force increment in direction of inter-particle vector
        //(note: xij[m]/rij is unit vector in inter-particle direction.)
        double df = xij[m] * dij;
        forces[i3 + m] += df;
        forces[j3 + m] -= df;
        virial -= xij[m] * df;
    }
}

double InteractionCalculator::getPotentialEnergy() const {
    return potentialEnergy;
}

double InteractionCalculator::getVirial() const {
    return virial;
}

const InstantaneousRadialDistribution& InteractionCalculator::getInstantaneousRadialDistribution() const {
    return radialDistribution;
}
