#include "MDRun_pl.h"

// Pair list method
MDRun_pairlist::MDRun_pairlist(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter)
				: MDRun(parameters, out, trajectoryFileWriter) {
	neighbors.resize(par.numberAtoms);
}

inline int nearestInteger(double x) {
	return x > 0 ? static_cast<int>(x + 0.5) : static_cast<int>(x - 0.5);
}

double MDRun_pairlist::calculateSquaredDistance(const std::vector<double>& x, unsigned int i, unsigned int j) {
	double dist = 0;
	for (unsigned int k = 0; k < 3; k++) {
		double diff = x[3*i + k] - x[3*j + k];
		double rdist = diff - nearestInteger(diff / par.boxSize[k]) * par.boxSize[k];
		dist += rdist * rdist;
	}
	return dist;
}

void MDRun_pairlist::calculateNeighbors(const std::vector<double> &x) {
	double cutoff2 = par.neighborhoodRadius * par.neighborhoodRadius;
	unsigned int natoms = par.numberAtoms;

	for (unsigned int i = 0; i < natoms - 1; i++) {
		neighbors[i].clear();
		for (unsigned int j = i + 1; j < natoms; j++) {
			double dist2 = calculateSquaredDistance(x,i, j);
			if (dist2 < cutoff2)
				neighbors[i].push_front(j);
		}
	}
}

void MDRun_pairlist::run(std::vector<double> &x, std::vector<double> &v) {
    forces.resize(x.size());
    synchronizedPositions.resize(x.size());
    radialDistribution.setZero();

    initializeVariables();
    initializeTemperature(v);

    output.printInitialTemperature(properties[1] / fac);
    output.printIterationStart();

    /* dynamics step */
    double time = par.initialTime;
    for (int nstep = 0; nstep < par.numberMDSteps; nstep++) {
        time += par.timeStep;
	// Every n-th step, update neighborhood tables
        if (nstep % par.nPairlist == 0) {
		calculateNeighbors(x);
	}
        performStep(x, v, nstep, time);
    }

    printAverages(time);

}

void MDRun_pairlist::calculateForces(std::vector<double>& x) {
	forceCalculator.calculate(x, forces, neighbors);
}

