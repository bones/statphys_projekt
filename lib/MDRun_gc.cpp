#include "MDRun_gc.h"

#include <cmath>

// Grid-cell method
MDRun_gc::MDRun_gc(const MDParameters& parameters, MDRunOutput& out, TrajectoryFileWriter& trajectoryFileWriter)
				: MDRun(parameters, out, trajectoryFileWriter) {
	// Set size of each cell, then compute number of cells
	for (unsigned i = 0; i < 3; ++i) {
		rcell[i] = par.boxSize[i] / std::ceil(par.boxSize[i] * 2. / par.sigmaLJ);
		Lcells[i] = par.boxSize[i] / rcell[i];
		cellRange[i] = std::ceil(par.interactionCutoffRadius / rcell[i]);
	}
	ncells = Lcells[0]*Lcells[1]*Lcells[2];
	cells.resize(ncells);

	resetCells();
}

inline void MDRun_gc::resetCells() {
	std::fill(cells.begin(), cells.end(), emptyCellFlag);
}

void MDRun_gc::run(std::vector<double> &x, std::vector<double> &v) {
    forces.resize(x.size());
    synchronizedPositions.resize(x.size());
    radialDistribution.setZero();

    initializeVariables();
    initializeTemperature(v);

    output.printInitialTemperature(properties[1] / fac);
    output.printIterationStart();

    /* dynamics step */
    double time = par.initialTime;
    for (int nstep = 0; nstep < par.numberMDSteps; nstep++) {
        time += par.timeStep;

	fillCells(x);

        performStep(x, v, nstep, time);
    }

    printAverages(time);
    std::cout << "Number of interactions calculated: " << forceCalculator.numInteractions << std::endl;
}

void MDRun_gc::fillCells(const std::vector<double> &x) {
	resetCells();
	
	for (int i = 0; i < par.numberAtoms; i++) {
		unsigned int c = computeCellFromAtom(x, i);
		cells[c] = i;
	}
}

unsigned int MDRun_gc::computeCellFromAtom(const std::vector<double>& x, const unsigned int& atom) {
	// Cell coordinate vector
	unsigned int c[3];
	for (unsigned int j = 0; j < 3; ++j) {
		double dim = x[3*atom + j];
		c[j] = static_cast<int>(dim / rcell[j]) % Lcells[j];
	}

	// Cell index from vector
	return c[0] * Lcells[1] * Lcells[2] + c[1] * Lcells[2] + c[2];
}

void MDRun_gc::calculateForces(std::vector<double>& x) {
	forceCalculator.calculate(x, forces, cells, emptyCellFlag, Lcells, cellRange);
}

