# Molecular Dynamics Simulation

Project by Sean Bone, Mikael Stellio 

This project extends the MD simulation library provided by the Statistical Physics and Computer Simulation course (FS18) as described in section 6.4 of the [accompanying script](https://gitlab.ethz.ch/bones/statphys_projekt/blob/polymorph/PDF/00_mdatom_script.pdf).

## Status

 - All three methods have been implemented. None of the implementations is able to _exactly_ reproduce the results of the "normal" simulation, but all of them are close.
 - Test 7 has a very large box and only 1000 atoms. This is the only one all of the implementations fail. Could the issue be related to the very low atom density?
 - Current test results:
    - Pair-list method: fails test 7 by 34.3%, fails test 5 by 1.24%
    - Linked-list method: fails test 7 by 2.71%
    - Grid-cell method: fails test 7 by 2.26% and test 5 by 1.02%

## Compiling and testing

The main `Makefile` in the project root has targets to compile and test the different methods:

- `bn`: build normal simulation
- `bpl`: build with pair-list approach
- `bll`: build with linked-list approach
- `bgc`: build with grid-cell approach
- `test`: run test suite on whatever is currently in build directory
- `ptest`: run same test in parallel
- `clean`: remove build directory entirely

## Tests
The first 6 tests are simply the exercises given in the project script.
Test 7 has a very large box size but only 1000 atoms.
Test 8 has a somewhat larger box size than normal but proportionately more atoms (8000 atoms in a 20x20x20 box), and fewer steps (to make running it faster...).

Each test is compared against the output of the normal method, and the results are considered correct if they are within 1% relative error of the expected result.

## TODO
 - ~~Grid-cell technique~~
 - ~~Plotting of runtime versus number of atoms~~
 - ~~Implement correctness testing in test suite~~
 - Find source of error in each method
 - Find a method for automatic generation of sensible parameter files
 - ...

## Project description: searching neighbor atoms

In  MD  simulations  the  bulk  of  the  computer  time  is  used  for  calculating  of  the non-bonded forces, that is, for finding the nearest neighbor atoms and subsequently evaluating  the  interaction  terms  for  the  obtained  atom  pairs.   Therefore,  various schemes for performing this task as efficiently as possible have been proposed and will be implemented in this project:

 - Atom pair-list technique
	- L. Verlet. [Computer ”experiments” on classical fluids. i. thermodynamical properties of lennard-jones molecules.](https://gitlab.ethz.ch/bones/statphys_projekt/blob/polymorph/PDF/01_verlet.pdf) (also [online version](https://journals.aps.org/pr/abstract/10.1103/PhysRev.159.98)) Phys. Rev., 159:98–103, 1967
 - Linked-list technique
	-  R. W. Hockney, S. P. Goel, and J. W. Eastwood. [Quiet high-resolution computer models of a plasma.](https://gitlab.ethz.ch/bones/statphys_projekt/blob/polymorph/PDF/02_hockney.pdf) (also [online version](https://www.sciencedirect.com/science/article/pii/21999174900101?via%3Dihub)) J. Comput. Phys., 14:148–158, 1974 
 - Grid-cell technique
	-  B. Quentrec and C. Brot. [New method for searching for neighbors in molecular dynamics computations.](https://gitlab.ethz.ch/bones/statphys_projekt/blob/polymorph/PDF/03_quentrec.pdf) (also [online version](https://www.sciencedirect.com/science/article/pii/0021999173900466)) J. Comput. Phys., 13:430–432, 1973


<!--
### Compiling and selecting the neighbor search technique

In order to select which neighbor search technique to use when compiling the program, compile the program with:

	cd build && cmake -D<method>=ON .. && make -j 8

Where `<method>` can be one of:

- `PAIR_LIST`: use atom pair-list technique
- `LINKED_LIST`: linked-list technique
- `GRID_CELL`: grid-cell techinque

#### Automatic compilation

Alternatively, you can use the Makefile in the root of the project:

    make bn    # Build normal program
    make bpl   # Build pair-list version
    make bll   # Build linked-list version
    make test  # Test whatever version is currently built
    make clean # Clean up build files

### Running tests
To run the test suite, compile the libary as above. Then use `make test` to run the tests. `make CTEST_OUTPUT_ON_FAILURE=1 test` will run the tests _and_ show the output of failed ones.
**Note:** the tests only check if the program runs, not correctness of the results. Test output and logs can be found under `build/Testing/Temporary/`.
 -->
