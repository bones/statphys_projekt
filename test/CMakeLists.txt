cmake_minimum_required(VERSION 3.1)

# Automated generation of multiple tests in one executable

# Based on:
# https://baoilleach.blogspot.ch/2013/06/using-ctest-with-multiple-tests-in.html
# Original CMakeLists.txt:
# https://github.com/openbabel/openbabel/blob/master/test/CMakeLists.txt

# Libraries to be used:
set(libs mdatomLib)

# List of test names. Files should be named accordingly: <test_name>_test.cpp
set (cpptests
     basic)

# List of test parts
# Each test will be iteratively called with a different "part" as argument each time
# If left blank, default_test_parts will be used
# E.g.:  set (popsim_basic_parts 1)


set(default_test_parts ex1 ex2 ex3 ex4 ex5 ex6 ex7 ex8) # ex9)
# For tests where a list of parts has not been defined we add a default (based on default_test_parts):
foreach(cpptest ${cpptests})
  if(NOT DEFINED "${cpptest}_parts")
     set(${cpptest}_parts "${default_test_parts}")
  endif()
endforeach()

# Add the .cpp files for each test
#  The file name is exptected to be <test_name>_test.cpp
foreach(cpptest ${cpptests})
  set(cpptestsrc ${cpptestsrc} ${cpptest}_test.cpp)
endforeach()

# Have CMake generate a single test_runnner executable, including all the above *_test.cpp files.
# We'll be able to run tests by calling this executable along with the corresponding test and subtest:
#   ~$ ./test_runner genome_copy_test 32
# Docs: https://cmake.org/cmake/help/cmake2.6docs.html#command:create_test_sourcelist
create_test_sourcelist(srclist test_runner.cpp ${cpptestsrc})
add_executable(test_runner ${srclist})
target_link_libraries(test_runner ${libs})


# Now add an automated test by iterating over the tests and their parts:
foreach(cpptest ${cpptests})
  foreach(part ${${cpptest}_parts})
    add_test(NAME test_${cpptest}_${part}
            COMMAND test_runner ${cpptest}_test ${part})
  endforeach()
endforeach()
