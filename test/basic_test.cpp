#include <MDSimulation.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <cmath>


// Replace occurrences of form with to in str
bool str_replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}


// Check if a file exists
inline bool file_exists (const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}

/*!
 * main function.
 * The program expects one argument for the input file (parameters), an one optional argument for
 * the  file with the initial coordinates.
 */

int basic_test(int argc, char* argv[]) {
    switch (argc) {
        case 2: break;
        case 3: break;
        default:
        std::cerr << "Usage: mdatom input_file [coordinate_file] > output \n";
            return 1;
    }
    
    std::string inputName = argv[1];
    
    std::string parameterFile = "../../test/input_files/%/params.inp";
    std::string coordinatesFile = "../../test/input_files/%/coords.inp";
    std::string outtestFile = "../../test/input_files/%/out_test.txt";
    std::string outputFile = "out_%.txt";
    
    // Replace % with inputName
    str_replace(parameterFile, "%", inputName);
    str_replace(coordinatesFile, "%", inputName);
    str_replace(outtestFile, "%", inputName);
    str_replace(outputFile, "%", inputName);
    
    // Check if coords file exists at all
    if (!file_exists(coordinatesFile)) {
        coordinatesFile = "";
    }
    
    // std::ofstream output(outputFile, std::ios_base::app);
    // Appending was giving issues if tests were previously interrupted
    std::ofstream output(outputFile);
    //~ std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    //~ std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!

    #ifdef PAIR_LIST
        std::cout << "ATOM PAIR LIST!!" << std::endl;
        MDS_pairlist md(output);
    #elif defined LINKED_LIST
        std::cout << "LINKED LIST!!" << std::endl;
		MDS_ll md(output);
    #elif defined GRID_CELL
        std::cout << "GRID CELL!!" << std::endl;
		MDS_gc md(output);
    #else
        std::cout << "STD::VECTOR DOUBLE!!" << std::endl;
        MDSimulation md(output);
    #endif

    try {
        md.performSimulation(parameterFile, coordinatesFile);
    }
    catch (std::exception& e) {
        std::cerr << e.what();
        return 1;
    }
    
    //~ std::cout.rdbuf(coutbuf); //reset to standard output again
    std::ifstream out_test, out;
    std::string line_test, line;
    
    out_test.open(outtestFile);
    out.open(outputFile);
    
    while (!out_test.eof()){
		getline(out_test,line_test);
		std::stringstream sslt(line_test);
		getline(out,line);
		std::stringstream ssl(line);
		double byte_lt, byte_l;
		while ( sslt >> byte_lt){
			ssl >> byte_l;
			
			if ( abs(byte_l - byte_lt)/byte_lt > 0.01 ){
				throw std::logic_error ("\n----------------------\nError in:  " + line + 
										"\nShould be: " + line_test +
										"\n----------------------\nProblem is: ( " + std::to_string(byte_l) + " - " + std::to_string(byte_lt) + " )/" + std::to_string(byte_lt) + " = " + std::to_string(abs(byte_l - byte_lt)/byte_lt) + " > 0.01");
				break;	
			}
		}
	}
	out_test.close();

    return 0;
}
